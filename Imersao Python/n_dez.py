#10.1 
for lista in range(1,21): 
  print(lista) 

#10.2 
lista =[]
for numero in range(1, 1000001):
    lista.append(numero)
    print(numero)

#10.3 
lista =[]
for numero in range(1, 1000001):
    lista.append(numero)
    print(numero) 
print(min(lista)) 
print(max(lista)) 
print(sum(lista)) 
lista =[valor**3 for valor in range(1,101)] 
print(lista)

#10.4  
lista =[]
for numero in range(1,21,2):
    lista.append(numero)
    print(numero) 

#10.5 
lista =[]
for numero in range(3,1001,3):
    lista.append(numero)
print(lista) 

#10.6 
lista = [] 
for valor in range(1,101): 
  numero = valor**3 
  lista.append(numero) 
print(lista)

#10.7 
lista =[valor**3 for valor in range(1,101)] 
print(lista)



