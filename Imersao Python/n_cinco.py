#5.1
#Na divisao se sobra 1 e impar, se nao sobrar nada e par
numero = int(input("PAR OU IMPAR? "))
resultado = numero % 2
if resultado == 0:
    print("O número é {} PAR".format(numero))
if resultado == 1:
    print("O número é {} ÍMPAR".format(numero)) 


#5.2
numero = int(input("Digite um número: "))
total = 0
for counter in range(1, numero + 2):
    if numero % counter == 0:
        total += 1

if total == 2:
    print("É PRIMO!")
if total != 2:
    print("NÃO É PRIMO!") 

#5.3 

numero_um = int(input("Primeiro Numero: ")) 
numero_dois = int(input("Segundo Numero: ")) 

adicao = numero_um + numero_dois 
subtracao = numero_um - numero_dois 
divisao = numero_um / numero_dois 
multiplicacao = numero_um * numero_dois  

print("O valor da adição é", adicao)
print("O valor da subtração é", subtracao)
print("O valor da multiplicação é", multiplicacao)
print("O valor da divisão é", divisao)  

#5.4 
idade = int(input("Idade: ")) 
semestre = int(input("Semestre: ")) 
duracao_curso = int(input("Duracao do curso (SEMESTRES): "))

tempo_restante = duracao_curso - semestre 
idade_final = idade + tempo_restante 

print("Voce ira acabar seu curso com ", idade_final, " anos! ")

#5.5 
idade = int(input("Idade: ")) 
semestre = int(input("Semestre: ")) 
duracao_curso = int(input("Duracao do curso (SEMESTRES): ")) 
atrasado = int(input("Voce esta algum semestre atrasado? Se sim, quantos?")) 

tempo_restante = (duracao_curso + atrasado) - semestre 
idade_final = idade + tempo_restante 

print("Voce ira acabar seu curso com ", idade_final, " anos! ")




