#2.1
amount_onions = int(input("Quantidade:"))
price_onions = 3.50

total = amount_onions * price_onions

print("Voce ira pagar R${} pelas cebolas.".format(total)); 

#2.2 
#Variavel determinando a quantidade de cebolas
cebolas = 300 
#Variavel determinando a quantidade de cebolas na caixa
cebolas_na_caixa = 120 
#Variavel determinando a quantidade de espaco na caixa
espaco_caixa = 5 
#Variavel quantidade de caixas
caixas = 60 
#Variavel determinando quantidade de cebolas fora da caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa 
#Variavel determinando quantidade de caixas vazias
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) 
#Variavel determiando quantidade de caixas necessarias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa 
#Quantidade de cebolas nas caixas
print("Existem {} cebolas encaixotadas".format(cebolas_na_caixa))
#Quantidade de cebolas fora da caixa
print("Existem {} cebolas sem caixa".format(cebolas_fora_da_caixa)) 
#Quantas cebolas cabem em cada caixa
print("Em cada caixa cabem {} cebolas".format(espaco_caixa))
#Quantas caixas vazias ainda temos
print("Ainda temos {} caixas vazias".format(caixas_vazias)) 
#Quantas caixas precisamos para empacotar todas as cebolas
print("Então, precisamos de {} caixas para empacotar todas as cebolas".format(caixas_necessarias)) 

#2.3 
cebolas = int(input("Quantidade de cebolas:"))
cebolas_na_caixa = int(input("Quantidade de caixas:"))
espaco_caixa = int(input("Espaco na caixa:"))
caixas = int(input("Quantidade de caixas:")) 

cebolas_fora_da_caixa = cebolas - cebolas_na_caixa 
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) 
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa 

print("Existem {} cebolas encaixotadas".format(cebolas_na_caixa))
print("Existem {} cebolas sem caixa".format(cebolas_fora_da_caixa)) 
print("Em cada caixa cabem {} cebolas".format(espaco_caixa))
print("Ainda temos {} caixas vazias".format(caixas_vazias)) 
print("Então, precisamos de {} caixas para empacotar todas as cebolas".format(caixas_necessarias))  
