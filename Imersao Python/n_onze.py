#11.1 
eu = ["droga","arara","roma","batata","corinthians"] 

amigo = eu[:3] 
amigo_dois = eu[2:] 
amigos_tres = eu[1:4]

print("Os 3 primeiros da lista sao ", amigo)  
print("Os 3 ultimos da lista sao ", amigo_dois) 
print("Os 3 do meio da listao sao", amigos_tres) 

#11.2 
pizza_hut=["calabresa","banana","frango","portuguesa"] 

pizza_hut_paraguai = pizza_hut[:]


pizza_hut.append("chocolate") 
pizza_hut_paraguai.append("carne seca")

print(pizza_hut) 
print(pizza_hut_paraguai) 
print("A do paraguai parece ser melhor.")

#11.3 
pizza_hut=["calabresa","banana","frango","portuguesa"] 
print('\033[1m' + "CARDAPIO PIZZA HUT") 
for cardapio in pizza_hut[:]:
 print(cardapio.title()) 

cachorro_quente= ["especial","sem salsicha","xcachorrotudao"] 
print('\033[1m' + "\n\n\nCACHORRO QUENTE DA TIA")
for cachorro in cachorro_quente[1:]: 
  print(cachorro.title())
print('\033[1m' + "\n\nInfelizmente estamos sem o cachorro quente especial!")
