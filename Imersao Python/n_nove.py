#9.1 
desaparecidos = ["chave", "oculos","computador","carteira"] 
for desaparecido in desaparecidos: 
 desaparecidos.remove()
  
#9.2 
lista = [1, 2, 3, 4, 5]
multiplicacao = 1
for numero in lista:
    multiplicacao *= numero
print(multiplicacao)

#9.3 
numero = [100,450,300,10,5,8,9]
print(min(numero)) 

#9.4 
nomes = ["pedro","jorge","caio","thomas"]  
nomes_dois = ["caio","thomas","eduarda","sirlene"] 
nomes_iguais = [] 
 
for nome in nomes: 
  if nome in nomes and nome in nomes_dois: 
    nomes_iguais.append(nome) 
print(nomes_iguais) 

#9.5 
nomes = ["pedro","jorge","caio","thomas","amanda","bruna"] 

#SORTED USADO PARA ORGANIZAR A LISTA SEM MUDAR SUA FORMA ORIGINAL
print(sorted(nomes)) 
#FORMA ORIGINAL
print(nomes) 

#9.6 
nomes = ["pedro","jorge","caio","thomas","amanda","bruna"] 
numero_letras = [] 

for nome in nomes: 
  tamanho = len(nome) 
  numero_letras.append(tamanho) 
print(numero_letras) 

#9.7 
lista = ["droga","arara","roma","batata","corinthians"] 
#palavra que vai indicar o anagrama
palavra = "amor" 
passo_um = sorted(palavra)
for anagrama in lista:
    passo_dois = sorted(anagrama)
    if passo_um == passo_dois:
      print(anagrama)


