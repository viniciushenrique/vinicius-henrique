from Entidades.roupas import Roupas

class Camisa(Roupas):
  def __init__(self,tipoDeGola = "fechada"):
    super().__init__()
    self._tipoDeGola = tipoDeGola

  @property
  def tipoDeGola(self):
    return self._tipoDeGola

  @tipoDeGola.setter
  def tipoDeGola(self,tipoDeGola):
    self._tipoDeGola = tipoDeGola

  
  def __str__(self):
  #  self._identificador = identificador
  #   self._tamanho = tamanho
  #   self._raca = raca
  #   self._cor = cor
  #   self._peso = peso
    return '''
    ---- Camisa ---- 
    Identificador: {}
    Tamanho: {}
    Modelo : {}
    Cor : {}
    Tipo de Gola: {}
    '''.format(self.identificador,self.tamanho
    ,self.modelo,self.cor,self.tipoDeGola)