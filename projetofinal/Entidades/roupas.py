class Roupas:
  
  def __init__(self,identificador = 0, tamanho=0, modelo="",cor=""):
    self._identificador = identificador
    self._tamanho = tamanho
    self._modelo = modelo
    self._cor = cor

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def tamanho(self):
    return self._tamanho

  @tamanho.setter
  def tamanho(self,tamanho):
    self._tamanho = tamanho

  @property
  def modelo(self):
    return self._modelo

  @modelo.setter
  def modelo(self,modelo):
    self._modelo = modelo

  @property
  def cor(self):
    return self._cor

  @cor.setter
  def cor(self,cor):
    self._cor = cor