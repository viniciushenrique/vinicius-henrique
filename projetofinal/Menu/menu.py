from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.camisa import Camisa

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar 
            """)
        return Validador.validar("[0-4]",
        """Opcao do menu deve estar entre {}""",
        """Opcao {} Valida!""")
        

    @staticmethod
    def menuConsultar():
        return input("""
                0 - Voltar
                1 - Consultar por identificador
                2 - Consultar por propriedade
                    """)

    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "2":
                        print("Entrou no menu CON PRO")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""            
            elif opMenu == "2":
                print("Entrei em Inserir")
                Menu.menuInserir(d)
                opMenu = ""
            elif opMenu == "3":
                print("Entrei em Alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado")

                    elif opMenu == "2":
                        print("Entrou no menu CON PRO")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = ""
            elif opMenu == "4":
                print("Entrei em Deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        Menu.menuDeletar(d,retorno)
                        
                    elif opMenu == "2":
                        Menu.menuBuscarPorAtributo(d)
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!") 
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            else:
                print("Digite uma opcao valida!")

    @staticmethod
    def menuInserir(d):
        camisa = Camisa()
        camisa.tamanho = input("Informe o tamanho:")
        camisa.modelo =  input("Informe o modelo:")
        camisa.tipoDeGola = input("Informe o Tipo de Gola:")
        camisa.cor = input("Informe cor:")
        d.inserirDado(camisa)
    
    @staticmethod
    def menuAlterar(retorno,d): 
        print(retorno)
        retorno.tamanho = Validador.validarValorInformado(retorno.tamanho,"Informe o tamanho:") 
        retorno.modelo = Validador.validarValorInformado(retorno.modelo,"Informe a modelo:") 
        retorno.tipoDeGola = Validador.validarValorInformado(retorno.tipoDeGola,"Informe o tipo de Gola:") 
        retorno.cor = Validador.validarValorInformado(retorno.cor,"Informe a cor:") 
        
        d.alterar(retorno)

    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input(
            """ Deseja deletar a camisa?
        S - Sim 
        N - Nao""")
        if (resposta == "S" or resposta == "s"):
            d.deletar(entidade)
            
     @staticmethod
    def menuBuscaPorIdentificador(d):        
        retorno = d.buscarPorIdentificador(
            Validador.validar(r'\d+','',''))
        return retorno

    @staticmethod
    def menuBuscarPorAtributo(d):
        retorno = d.buscarPorAtributo(
            input("Informe um tamanho"))
        print(retorno)

