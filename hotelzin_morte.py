#MEU GRANDE SONHO É ESSE CÓDIGO FUNCIONAR (21/08/2019 as 16:33)
import sys 
#"Renato Faca", "Roberto Arma", "Maria Calice","Roberta Corda"] 


def askName():
	return input("Nome do suspeito: ")

def validateString(name): 
	return not name.isnumeric	  

def validateVowels(name): 
    vowelCounter = 0
    for a in name:
        if a in "aei":
            vowelCounter += 1
    return vowelCounter >= 3  and "o" not in name.lower() and "u" not in name.lower()

def getBioGender(name): 
	mens = ["Renato Faca", "Roberto Arma", "Uesllei"]
	womans = ["Maria Cálice", "Roberta Corda"]
	if name in mens:
		return "M"
	elif name in womans:
		return "W"
	else:
		sys.exit("Gênero indefinido!")
  

def getWeapon(name): 
  haveWhiteWeapon = False
  whiteWeapon = ["Faca", "Calice", "Corda"] 
  for x in whiteWeapon: 
    if x in name.lower():  
        haveWhiteWeapon = True 
  return haveWhiteWeapon  

def validateAssassinWoman(bioGender, weapon): 
    return bioGender == "W" and getWeapon 

def validateAssassinMan(bioGender, time):
    return bioGender == "M" and time == "00:30"


# solcitar nome ao usuário
name = askName()

# verifica se o nome é uma string
vn = validateString(name)

if not vn:
	sys.exit("Erro no nome.")

# verifica se nome respeita a RN1 (vogais)
vv = validateVowels(name)

if not vv:
	sys.exit("Não é o suspeito!")

time = ""
if getBioGender == "M":
    time = input("Que horas você estava no saguão? (hh:mm)")

bioGender = getBioGender(name)
weapon    = getWeapon(name)

vw = validateAssassinWoman(bioGender, weapon)
vm = validateAssassinMan(bioGender, time)

if not(vw and vm):
	sys.exit("Não é o suspeito!")

print("FOOOOOOOIIIII VOCEEEEEEEEEEEE QUE MATOOOOOOOOOUUU!!!!!!!!!!!!!!")